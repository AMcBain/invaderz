#include <Arduboy.h>

Arduboy arduboy;

#define ALIENS_PER_ROW 6
#define ALIEN_WIDTH 12
#define ALIEN_HEIGHT 8
#define ALIEN_H_SPACE 4
#define ALIEN_V_SPACE 4

const uint8_t PROGMEM alienA0[] = { B00000000, B00000000, B00000000, B00000000, B01011000, B10111100, B00010110, B00111111, B00111111, B00010110, B10111100, B01011000 };
const uint8_t PROGMEM alienA1[] = { B00000000, B00000000, B00000000, B00000000, B10011000, B01011100, B10110110, B01011111, B01011111, B10110110, B01011100, B10011000 };

const uint8_t PROGMEM alienB0[] = { B00000000, B01110000, B00011000, B01111101, B10110110, B10111100, B00111100, B10111100, B10110110, B01111101, B00011000, B01110000 };
const uint8_t PROGMEM alienB1[] = { B00000000, B00011110, B10111000, B01111101, B00110110, B00111100, B00111100, B00111100, B00110110, B01111101, B10111000, B00011110 };

const uint8_t PROGMEM alienC0[] = { B00011100, B01011110, B11111110, B10110110, B00110111, B01011111, B01011111, B00110111, B10110110, B11111110, B01011110, B00011100 };
const uint8_t PROGMEM alienC1[] = { B10011100, B10011110, B01011110, B01110110, B00110111, B01011111, B01011111, B00110111, B01110110, B01011110, B10011110, B10011100 };

#define SHIP_WIDTH 13
#define SHIP_HEIGHT 2
int8_t ship = WIDTH / 2 - SHIP_WIDTH / 2;

#define NO_MISSILE -1
#define MISSILE_FRAME 1
uint8_t missileX = 0;
int8_t missileY = NO_MISSILE;

#define HOUSE_WIDTH 13
#define HOUSE_HEIGHT 4
#define HOUSE_H_SPACE 14

const uint8_t PROGMEM house[] = { B00001100, B00001110, B00001111, B00000111, B00000111, B00000111, B00000111, B00000111, B00000111, B00000111, B00001111, B00001110, B00001100 };
uint8_t houseXOffset = (WIDTH - HOUSE_WIDTH * 4 - HOUSE_H_SPACE * 3) / 2;
uint8_t houseYOffset = HEIGHT - 5 - SHIP_HEIGHT - HOUSE_HEIGHT;

bool damage[WIDTH][4];

#define NO_ALIEN 255
uint8_t aliens[ALIENS_PER_ROW * 3][2];
uint8_t alienCount = ALIENS_PER_ROW * 3;

#define NO_BOMB 255
uint8_t bombs[WIDTH];

uint8_t minX = 0;
uint8_t maxX = (WIDTH - (ALIEN_WIDTH + ALIEN_H_SPACE) * ALIENS_PER_ROW + ALIEN_H_SPACE);

uint8_t xOffset = maxX / 2;
uint8_t yOffset = 0;

uint8_t lastYOffset = 0;

bool drop = false;
int8_t dir = 1;

uint8_t frame = 0;

// This runs once on startup.
void setup ()
{
  arduboy.begin();

  // Keep count of invaders in a separate variable used to feed to everyXframes to make it go faster when there's less of them
  // It seems that as they disappear the box of invaders shrinks and they can go left or right more
  uint8_t x = 0;
  for (uint8_t i = 0; i < ALIENS_PER_ROW; i++)
  {
    aliens[i][0] = x;
    aliens[i][1] = 0;
    aliens[i + ALIENS_PER_ROW][0] = x;
    aliens[i + ALIENS_PER_ROW][1] = ALIEN_HEIGHT + ALIEN_V_SPACE;
    aliens[i + ALIENS_PER_ROW * 2][0] = x;
    aliens[i + ALIENS_PER_ROW * 2][1] = (ALIEN_HEIGHT + ALIEN_V_SPACE) * 2;
    x += ALIEN_WIDTH + ALIEN_H_SPACE;
  }

  // Initialize missiles. Dosn't matter what data type I use as long as I always use NO_MISSILE
  for (x = 0; x < WIDTH; x++)
  {
    bombs[x] = NO_MISSILE;
  }

  draw();
}

// This runs often so you can update things.
void loop ()
{
  if (!arduboy.nextFrame())
    return;

  if (arduboy.pressed(LEFT_BUTTON))
  {
    ship = max(0, ship - 1);
  }
  else if (arduboy.pressed(RIGHT_BUTTON))
  {
    ship = min(WIDTH - SHIP_WIDTH, ship + 1);
  }

  // Destroys the houses too fast?
  if (arduboy.everyXFrames(MISSILE_FRAME * 2) && arduboy.pressed(A_BUTTON) && missileY == NO_MISSILE)
  {
    missileX = ship + SHIP_WIDTH / 2;
    missileY = HEIGHT - SHIP_HEIGHT - 5;
  }

  if (arduboy.everyXFrames(MISSILE_FRAME) && missileY != NO_MISSILE)
  {
    missileY--;

    for (uint8_t i = 0; alienCount > 0 && i < ALIENS_PER_ROW * 3; i++)
    {
      if (aliens[i][0] + xOffset <= missileX && missileX < aliens[i][0] + xOffset + ALIEN_WIDTH && aliens[i][1] + yOffset + ALIEN_HEIGHT >= missileY && missileY > aliens[i][1] + yOffset)
      {
        aliens[i][0] = NO_ALIEN;
        aliens[i][1] = NO_ALIEN;
        missileY = NO_MISSILE;
        alienCount--;
        break;
      }
    }

    if (missileY != NO_MISSILE && missileY >= houseYOffset && arduboy.getPixel(missileX, missileY) != 0)
    {
      damage[missileX][missileY - houseYOffset] = true;
      missileY = NO_MISSILE;
    }
  }

  // It's a magic number! Mwahahahaha. Actually no. It's what you get when
  // you solve 18^x = 120 for x, then round/truncate into an approximation.
  if (alienCount > 0 && arduboy.everyXFrames((uint8_t)pow(alienCount, 1.6564)))
  {
    if (lastYOffset == yOffset && (xOffset == minX || xOffset == maxX))
    {
      lastYOffset = yOffset;
      dir = dir < 0 ? 1 : -1;
      yOffset++;
    }
    else
    {
      lastYOffset = yOffset;
      xOffset += dir;
    }
  }

  draw();
}

uint8_t getMissile ()
{
  return HEIGHT - (missileY + 4);
}

void draw ()
{
  arduboy.clear();

  bool drawHouses = true;
  for (uint8_t i = 0; i < ALIENS_PER_ROW * 3; i++)
  {
    uint8_t x = aliens[i][0] + xOffset;
    uint8_t y = aliens[i][1] + yOffset;

    if (aliens[i][1] != NO_ALIEN)
    {
      const uint8_t* bitmap = i < 6 ? alienA0 : (i < 12 ? alienB0 : alienC0);
  
      if (xOffset % 2 != 0)
      {
        bitmap = i < 6 ? alienA1 : i < 12 ? alienB1 : alienC1;
      }
  
      if (y + ALIEN_HEIGHT >= houseYOffset)
      {
        drawHouses = false;
      }
  
      arduboy.drawBitmap(x, y, bitmap, ALIEN_WIDTH, ALIEN_HEIGHT, WHITE);
    }
  }

  if (drawHouses)
  {
    for (uint8_t x = houseXOffset; x < WIDTH - houseXOffset; x += HOUSE_WIDTH + HOUSE_H_SPACE)
    {
      arduboy.drawBitmap(x, houseYOffset, house, HOUSE_WIDTH, HOUSE_HEIGHT, WHITE);
    }
  }

  for (uint8_t x = 0; x < WIDTH; x++)
  {
    if (bombs[x] != NO_BOMB)
    {
      arduboy.drawFastVLine(x, bombs[x], 4, WHITE);
    }
  }

  for (uint8_t x = 0; x < WIDTH; x++)
  {
    for (uint8_t y = 0; y < 4; y++)
    {
      if (damage[x][y])
      {
        arduboy.drawPixel(x, y + houseYOffset, BLACK);
      }
    }
  }

  if (missileY != NO_MISSILE)
  {
    arduboy.drawFastVLine(missileX, missileY, 4, WHITE);
  }

  arduboy.drawPixel(ship + SHIP_WIDTH / 2, HEIGHT - 2, WHITE);
  arduboy.drawFastHLine(ship, HEIGHT - 1, SHIP_WIDTH, WHITE);

  arduboy.display();
}

